const colors = require('tailwindcss/colors');

module.exports = {
  purge: ['./index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        sky: colors.sky,
        cyan: colors.cyan,
        'blue-gray': colors.blueGray,
        'dark-blue': {
          900: '#0C182B',
          800: '#14243D',
          500: '#00fff7',
          200: '#8bacda',
        },
      },
      fontFamily: {
        'red-hat': ['Red Hat Display', 'sans-serif'],
        'space-mono': ['Space Mono', 'monospace', 'sans-serif'],
        barlow: ['Barlow', 'sans-serif'],
        fraunces: ['Fraunces', 'sans-serif'],
      },
      backgroundImage: {
        'header-img': "url('./src/images/image-header.jpg')",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
